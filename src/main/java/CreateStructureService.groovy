import groovy.xml.MarkupBuilder
import wslite.http.auth.HTTPBasicAuthorization
import wslite.soap.SOAPClient
import wslite.soap.SOAPClientException

/**
 * Created by Insight on 27-03-2018.
 */
class  CreateStructureRequestService {

    static Map<String, List<Map>> out = null;


    public static void createConnectionUrl(def username, def password, def account,String orgCode, File file) {

        SOAPClient client = new SOAPClient("https:" + account + "/fscmService/StructureServiceV2?WSDL");
        client.setAuthorization(new HTTPBasicAuthorization(username, password));
        createMap(file);
        try {
            def resCreate = client.send(
                    connectTimeout: 10000,
                    readTimeout: 30000,
                    useCaches: false,
                    followRedirects: false,
                    sslTrustAllCerts: true, buildPayload(orgCode))
            if (resCreate.httpResponse.statusMessage == "OK") {
                println "BOM for item created sucessfully."
            } else {
                println "BOM creation for item  failed."
            }
        }catch(SOAPClientException ex){
            println "Response for item timed out, BOM may have beenn created sucessfully."
        }

    }
    public static  Map<String, List<Map>> createMap(file){
        out = new HashMap<>();
        file.readLines().each { l ->
            if (!l.startsWith('#')) {
                def line = l.split('\t')
                if (out.get(line[0]))
                    out.get(line[0]) << [cmp: line[1], rd: line[2], notes: line[3], critical: line[4]]
                else {
                    out[line[0]] = [[cmp: line[1], rd: line[2], notes: line[3], critical: line[4]]]
                }
            }
        }


    }

    public static buildPayLoad(String orgCode){
        def xmlWriter = new StringWriter()
        def xml = new MarkupBuilder(xmlWriter)
        xml.'soapenv:Envelope'('xmlns:soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
                'xmlns:typ': 'http://xmlns.oracle.com/apps/scm/productModel/items/structures/structureServiceV2/types/',
                'xmlns:str': 'http://xmlns.oracle.com/apps/scm/productModel/items/structures/structureServiceV2/',
                'xmlns:sub': 'http://xmlns.oracle.com/apps/scm/productModel/items/structures/flex/substituteComponent/',
                'xmlns:ref': 'http://xmlns.oracle.com/apps/scm/productModel/items/structures/flex/referenceDesignator/',
                'xmlns:com': 'http://xmlns.oracle.com/apps/scm/productModel/items/structures/flex/component/',
                'xmlns:str1': 'http://xmlns.oracle.com/apps/scm/productModel/items/structures/flex/structureHeader/') {
            'soapenv:Header'()
            'soapenv:Body' {
                'typ:createStructure' {
                    'typ:structure' {
                        'str:OrganizationCode'(orgCode)
                        'str:ItemNumber'('')
                        'str:StructureName'('Primary')
                        out.each { k, v ->
                            v.each{entry ->
                            if (entry.cmp) {
                                'str:Component' {
                                    'str:ComponentSequenceId'(entry.cmp)
                                    'str:ComponentItemNumber'(entry.cmp)
                                    if (entry.rd) {
                                        entry.rd.split(',').each { refDes ->
                                            'str:ReferenceDesignator' {
                                                'str:ReferenceDesignator'(refDes)
                                            }
                                        }
                                    }

                                    'str:ComponentDFF' {
                                        'com:yield'(1)
                                        if (entry.notes) {
                                            'com:bomComments'(entry.notes)
                                        }
                                        if (entry.critical != null || entry.critical != '') {
                                            'com:criticalComponent'(entry.critical)
                                        }
                                    }
                                }
                            }
                        }
                        }
                    }
                }
            }
        }
        xmlWriter.toString()
    }

}