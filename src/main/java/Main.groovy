/**
 * Created by Insight on 27-03-2018.
 */
import groovy.util.CliBuilder;

import java.io.File;

/**
 * Created by Insight on 27-03-2018.
 */
public class Main {

    public static void main(String[] args){

        CliBuilder cli = new CliBuilder(usage: "wsclient -u <username> -p <password> [-l <langu>] [-j <j1ICHID>]")
        cli.h(longOpt: 'help', 'usage information', required: false)

        cli.u(longOpt: 'username', 'username for Fusion', required: true, args: 1)
        cli.p(longOpt: 'password', 'password for Fusion', required: true, args: 1)
        cli.a(longOpt: 'account', 'account in UCM', required: true, args: 1)
        cli.o(longOpt: 'orgCode', 'account in UCM', required: true, args: 1)
        cli.w(longOpt: 'wsdl', 'location of wsdl', required: false, args: 1)



        def options = cli.parse(args)
        if (options) {
            File file = new File(options.f)
            if (file && file.canRead()) {
                    CreateStructureRequestService.createConnectionUrl(options.u,options.p,options.a,file)

            } else {
                println "File not found at $options.f"
            }
        }








    }


}

